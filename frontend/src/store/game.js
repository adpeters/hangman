import game_api from '../game_api';

import {
    ADD_GUESS,
    SET_GAME,
    SET_GAME_DETAIL,
} from './types';

const GAME_STORAGE_KEY = 'GAME_STORAGE_KEY';

const initialState = {
    game_id: null,
    game_state: "",
    current_guesses: [],
};

const getters = {
    guessesRemaining: function(state) {
        return 8 - state.current_guesses.length;
    },
}

const actions = {
    initialize({ dispatch, commit }, id) {
        const game_id = localStorage.getItem(GAME_STORAGE_KEY);

        if (id) {
            commit(SET_GAME, id);
            dispatch('get_game_detail', id);
        } else if (game_id) {
            console.log('here with ' + game_id);
            dispatch('get_game_detail', game_id);
        } else {
            dispatch('get_game_detail', null);
        }
    },
    get_game_detail({ commit }, id) {
        return new Promise((resolve, reject) => {
            game_api.get_game_detail(id)
                .then(({data}) => {
                    if (!this.state.game_id) {
                        commit(SET_GAME, data.id);
                    }
                    commit(SET_GAME_DETAIL, data);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },
    add_guess({ commit }, params) {
        return new Promise((resolve, reject) => {
            game_api.add_guess(params['id'], params['value'])
                .then(({ data }) => {
                    commit(ADD_GUESS, data.game_detail);
                    resolve(data.result);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },
    new_word({ commit }, id) {
        return new Promise((resolve, reject) => {
            game_api.new_word(id)
                .then(({ data }) => {
                    commit(SET_GAME_DETAIL, data);
                    resolve(data);
                })
                .catch(error => {
                    reject(error.response);
                });
        });
    },
    undo({ commit }, id) {
        return new Promise((resolve, reject) => {
            game_api.undo(id)
                .then(({ data }) => {
                    commit(SET_GAME_DETAIL, data);
                    resolve(data);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },
}

const mutations = {
    [SET_GAME](state, game) {
        localStorage.setItem(GAME_STORAGE_KEY, game);
        state.game_id = game;
    },
    [SET_GAME_DETAIL](state, game_detail) {
        state.game_id = game_detail.id;
        state.current_guesses = game_detail.current_guesses;
        state.game_state = game_detail.game_state;
    },
    [ADD_GUESS](state, game_detail) {
        state.current_guesses = game_detail.current_guesses;
        state.game_state = game_detail.game_state;
    },
}

export default {
    namespaced: true,
    state: initialState,
    getters,
    actions,
    mutations
};

