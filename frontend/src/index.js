import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'

new Vue({
  el: '#app',
  render: h => h(App),
  router,
  store,
  created: function() {
    console.log(this.$route.query.id);
    store.dispatch('game/initialize', this.$route.query.id);
  },
})
