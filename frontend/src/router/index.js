import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'
import App from '../App.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      component: App
    },
  ]
})
