import random
import uuid

WORDS = ['RABBIT','BUNNY','CARROT','LETTUCE','BURROW','FLUFFY','FLOPPY','LITTER','PELLETS']

class GameStore():
    def __init__(self):
        self.id = uuid.uuid4()
        self.words = random.sample(WORDS, len(WORDS))
        self.current_word_idx = 0
        self.guesses = {
            0: [],
        }
        self.bookmark = None

    @property
    def current_guesses(self):
        return self.guesses.get(self.current_word_idx, [])

    @property
    def game_state(self):
        current_word = self.words[self.current_word_idx]
        gs = self.current_guesses
        if self.has_word_guess:
            wg = [w for w in gs if len(w) > 1][0]
            if wg.lower() == current_word.lower():
                return current_word
            
        txt = ''
        for i in range(len(current_word)):
            letter = current_word[i]
            if letter in gs:
                txt += letter
            else:
                txt += '_'
            
        return txt

    @property
    def has_word_guess(self):
        g = self.current_guesses
        if len(g) == 0:
            return False
        return max([len(g) for g in self.guesses.get(self.current_word_idx, [])]) > 1

    def save_bookmark(self):
        word_guesses = self.guesses.get(self.current_word_idx, [])

        if len(word_guesses) == 0 and self.current_word_idx > 0:
            word_guesses = self.guesses.get(self.current_word_idx - 1, [])
        
        if len(word_guesses) == 0:
            return

        guess_idx = len(word_guesses) - 1
        self.bookmark = (self.current_word_idx, guess_idx)

    def pop_bookmark(self):
        if self.bookmark is None:
            return

        word, guess = self.bookmark

        for key in range(len(self.words), word, -1):
            self.guesses.pop(key, None)

        guesses = self.guesses.get(word, [])
        for ix in range(len(guesses), guess, -1):
            guesses.pop(ix, None)

        self.guesses[word] = guesses
        self.bookmark = None
    
    def add_guess(self, guess):
        guesses = self.guesses.get(self.current_word_idx, [])
        if len(guesses) == 8:
            return False

        if len(guess) > 1 and self.has_word_guess:
            raise Exception("You do not have any more full word guesses available.")

        guesses.append(guess)
        self.guesses[self.current_word_idx] = guesses

        is_winner = self.check_is_winner(self.current_word_idx)
        if is_winner:
            return True
        elif not is_winner and len(guesses) == 8:
            return False
        return None

    def undo(self):
        if self.current_word_idx is None:
            self.current_word_idx = max(self.guesses.keys())
        if len(self.guesses) == 0:
            return False
        word_guesses = self.current_guesses
        if len(word_guesses) == 0 and self.current_word_idx > 0:
            self.current_word_idx -= 1
            word_guesses = self.current_guesses

        if len(word_guesses) == 0:
            return False

        tt = word_guesses.pop()
        self.guesses[self.current_word_idx] = word_guesses
        return True
            
    def check_is_winner(self, word_idx):
        if self.has_word_guess:
            word_guess = [w for w in self.current_guesses if len(w) > 1][0]
            if word_guess.lower() == self.words[word_idx].lower():
                return True

        return not '_' in self.game_state

    def start_new_word(self):
        if len(self.current_guesses) < 8 and not self.check_is_winner(self.current_word_idx):
            return None
        if self.current_word_idx < len(self.words) - 1:
            self.current_word_idx += 1
            self.guesses[self.current_word_idx] = []
        else:
            self.current_word_idx = None
            return False

    def serialize(self):
        return {
            'id': self.id,
            'current_guesses': self.current_guesses,
            'game_state': self.game_state,
        }

