from flask import Flask, render_template, jsonify, current_app, request, abort
from flask_webpackext import FlaskWebpackExt, WebpackTemplateProject
from store import GameStore

app = Flask(__name__, template_folder="/public/dist", static_folder="/public/dist", static_url_path="")
app.url_map.strict_slashes = False
app.config.update({ 
    'WEBPACKEXT_MANIFEST_PATH': '/code/bundle.json',
})

FlaskWebpackExt(app)

global store
store = []

def get_game_by_id(id):
    if id is None:
        return None
    for g in store:
        if str(g.id) == id:
            return g
    return None

@app.route('/')
def main():
    return render_template('index.html')

@app.route('/api/games/')
def games():
    return jsonify([g.serialize() for g in store])

@app.route('/api/game/')
def game_status():
    id = request.args.get('id', None)
    game = get_game_by_id(id)
    if game is None or game.current_word_idx is None:
        game = GameStore()
        store.append(game)

    return jsonify(game.serialize())

@app.route('/api/game/guess/', methods=['POST'])
def guess():
    id = request.form.get('game', None)
    game = get_game_by_id(id)
    if game is None:
        abort(400)
    guess = request.form.get('guess', None)
    result = game.add_guess(guess)
    data = {
        'game_detail': game.serialize(),
        'result': result,
    }
    return jsonify(data)

@app.route('/api/game/new/')
def new_game():
    id = request.args.get('id', None)
    game = get_game_by_id(id)
    if game is None:
        game = GameStore()
        store.append(game)
    else:
        start = game.start_new_word()
        if start is False:
            return jsonify({ 'error': 'All words have been used. This game is over.' }), 403
    return jsonify(game.serialize())

@app.route('/api/game/undo/')
def undo():
    id = request.args.get('id', None)
    game = get_game_by_id(id)
    game.undo()
    return jsonify(game.serialize())
