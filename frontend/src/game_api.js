import session from './session';
import qs from 'qs';

export default {
    add_guess(game, guess) {
        let data = qs.stringify({ 'game': game, 'guess': guess });
        return session.post(`/api/game/guess/`, data);
    },
    get_game_detail(id) {
        if (id) {
            return session.get(`/api/game/?id=${id}`);
        } else {
            return session.get('/api/game/');
        }
    },
    new_word(id) {
        return session.get(`/api/game/new/?id=${id}`);
    },
    undo(id) {
        return session.get(`/api/game/undo/?id=${id}`);
    },
}
